package com.bouthaina.babbelcodechallengeproject

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.v7.widget.RecyclerView
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import com.bouthaina.babbelcodechallengeproject.dataModel.WordModel
import com.bouthaina.babbelcodechallengeproject.features.game.GameActivity
import com.bouthaina.babbelcodechallengeproject.features.game.GameActivityViewModel
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GameActivityTest {

    private lateinit var viewModel: GameActivityViewModel
    private lateinit var word: WordModel
    @Mock
    lateinit var mockActivity: GameActivity
    @Mock
    lateinit var mockRecycleView: RecyclerView
    @Mock
    lateinit var mockFrameLayout: FrameLayout
    @Mock
    lateinit var mockTextCurrentWord: TextView
    @Mock
    lateinit var mockLayout: LinearLayout
    @Mock
    lateinit var mockScoreNumber: TextView
    @Mock
    lateinit var mockTextFalling: TextView

    @Before
    fun  setup() {
     viewModel  = GameActivityViewModel(mockActivity,mockRecycleView, mockFrameLayout, mockTextCurrentWord, mockLayout, mockScoreNumber, mockTextFalling)
        Mockito.`when`(mockActivity.getViewModel()).thenReturn(viewModel)
    }

    @Test
    fun testGameActivityTest(){
        //given
        //when
        //then
        onView(withId(R.id.game_activity_Linearlayout_score)).check(matches(isDisplayed()))
        onView(withId(R.id.game_activity_blurView)).check(matches(isDisplayed()))
        onView(withId(R.id.game_activity_texthighscore)).check(matches(isDisplayed()))
    }
}