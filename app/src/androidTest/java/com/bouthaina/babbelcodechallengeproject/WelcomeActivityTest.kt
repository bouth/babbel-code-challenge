package com.bouthaina.babbelcodechallengeproject

import android.content.Context
import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withText
import com.bouthaina.babbelcodechallengeproject.application.Application
import com.bouthaina.babbelcodechallengeproject.dataModel.WordModel
import com.bouthaina.babbelcodechallengeproject.features.game.GameActivity
import com.bouthaina.babbelcodechallengeproject.features.welcome.WelcomeActivity
import com.bouthaina.babbelcodechallengeproject.features.welcome.WelcomeActivityViewModel
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class WelcomeActivityTest {

    private lateinit var viewModel: WelcomeActivityViewModel
    @Mock
    lateinit var mockActivity: WelcomeActivity

    @Before
    fun setup() {

        viewModel = WelcomeActivityViewModel(context = mockActivity)
        Mockito.`when`(mockActivity.getViewModel()).thenReturn(viewModel)
    }

    @Test
    fun testWelcomeActivityTest() {
        //given

        //when
        //then
        onView(ViewMatchers.withId(R.id.welcome_activity_hola)).check(matches(isDisplayed()))
                .check(matches(withText(Application.getContext().getString(R.string.hola))))
        onView(ViewMatchers.withId(R.id.welcome_activity_learn)).check(matches(isDisplayed()))
                .check(matches(withText(Application.getContext().getString(R.string.learn_spanish))))

        onView(ViewMatchers.withId(R.id.start_game_button)).check(matches(isDisplayed()))

        //when
        onView(ViewMatchers.withId(R.id.start_game_button)).perform(click())

        //then
        Mockito.verify(mockActivity).startActivity(Mockito.any())
    }
}