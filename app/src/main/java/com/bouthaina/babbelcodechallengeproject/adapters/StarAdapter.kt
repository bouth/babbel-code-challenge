package com.bouthaina.babbelcodechallengeproject.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bouthaina.babbelcodechallengeproject.R

/**
 * Adapter for a RecyclereView that displays stars .
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

class StarAdapter(var starts: Int?) : RecyclerView.Adapter<StarAdapter.HeartViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeartViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_star, parent, false)
        return HeartViewHolder(v)
    }

    override fun onBindViewHolder(holder: HeartViewHolder, position: Int) {}
    override fun getItemCount(): Int = starts!!

    class HeartViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
         var star: ImageView?=null
        init {
            star = itemView.findViewById<View>(R.id.item_star_Image) as ImageView
        }
    }
}
