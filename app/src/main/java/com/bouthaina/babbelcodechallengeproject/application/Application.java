package com.bouthaina.babbelcodechallengeproject.application;

import android.content.Context;

/**
 * Application which handles Services and other Setups before starting the first Activity
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */
public class Application extends android.app.Application {
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    public static Context getContext() {
        return context;
    }

}
