package com.bouthaina.babbelcodechallengeproject.application;

import com.bouthaina.babbelcodechallengeproject.dataModel.WordSetting;
import com.google.gson.Gson;

public class Singleton {

    private static Singleton singleton;

    private WordSetting wordSetting;
    private static final String WORD_SET_PREF_KEY = "wordsetprefkey";

    private int highScore;
    private static final String HIGH_SCORE_PREF_KEY = "highscoreprefkey";


    public WordSetting getWordSetting() {
        if(wordSetting ==null || wordSetting.isEmpty())
            wordSetting = new Gson().fromJson(Util.loadSharedPreference(Application.getContext(), WORD_SET_PREF_KEY, null), WordSetting.class);
        return wordSetting;
    }

    public void setWordSetting(WordSetting wordSetting) {
        this.wordSetting = wordSetting;
        Util.salvarSharedPreference(Application.getContext(), WORD_SET_PREF_KEY, new Gson().toJson(wordSetting));
    }

    public int getHighScore() {
        if(highScore==0)
            highScore = Integer.parseInt(Util.loadSharedPreference(Application.getContext(), HIGH_SCORE_PREF_KEY, String.valueOf(0)));
        return highScore;
    }

    public void setHighScore(int highScore) {
        Util.salvarSharedPreference(Application.getContext(), HIGH_SCORE_PREF_KEY, String.valueOf(highScore));
        this.highScore = highScore;
    }

    public Singleton() { }

    public static Singleton getSingleton(){
        if(singleton == null)
            singleton = new Singleton();
        return singleton;
    }

}
