package com.bouthaina.babbelcodechallengeproject.application;

import android.content.Context;
import android.content.SharedPreferences;


/**
 * ToolTip View
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 * @see <a href="https://github.com/sephiroth74/android-target-tooltip">https://github.com/sephiroth74/android-target-tooltip</a>
 */

public class Util {

    private static final String KEY = "sharedpreferences";

    public static void salvarSharedPreference(Context context, String chave, String dado){
        if(context==null) return;
        SharedPreferences shared = context.getSharedPreferences(KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString(chave, dado);
        editor.apply();
    }

    public static  void clearSharedPreference(Context context) {
        SharedPreferences shared = context.getSharedPreferences(KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.clear();
        editor.apply();
    }

    public static void deleteSharedPreference(Context context, String chave){
        SharedPreferences shared = context.getSharedPreferences(KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.remove(chave);
        editor.apply();
    }

    public static String loadSharedPreference(Context context, String chave, String padrao){
        SharedPreferences shared = context.getSharedPreferences(KEY, Context.MODE_PRIVATE);
        return shared.getString(chave, padrao);
    }


}
