package com.bouthaina.babbelcodechallengeproject.dataModel

import java.io.Serializable

/**
 * The model object for a   word model.
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

class WordModel : Serializable {

    lateinit var text_eng: String
    lateinit var text_spa: String

    companion object {
        fun isCorrectlyTranslated(firstWord: WordModel, secondWord: WordModel): Boolean {
            return firstWord.text_eng == secondWord.text_eng
        }
    }
}
