package com.bouthaina.babbelcodechallengeproject.dataModel

import com.bouthaina.babbelcodechallengeproject.application.Singleton

import java.io.Serializable
import java.util.ArrayList
import java.util.Random

/**
 * The model object to remove  falling word or get falling word .
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

class WordSetting : ArrayList<WordModel>(), Serializable {

    fun getTombstoneWord(currentWord: WordModel): WordModel {
        return if (random.nextInt() % 2 == 0) currentWord
        else  this[random.nextInt(this.size)]
    }
    fun removeWord(): WordModel {
        if (this.size == 0) this.addAll(Singleton.getSingleton().wordSetting)
        return this.removeAt(random.nextInt(this.size))
    }

    companion object {
        val random = Random(System.currentTimeMillis())
    }
}
