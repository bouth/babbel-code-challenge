package com.bouthaina.babbelcodechallengeproject.features

import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.test.filters.Suppress
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.WindowManager
import com.bouthaina.babbelcodechallengeproject.R

import java.util.Observable
import java.util.Observer

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper
import java.util.ArrayList

/**
 * BaseActivity for all Activities.
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

open class BaseActivity: AppCompatActivity(), Observer {

    protected fun setStatusBarAndNavBarTranslucent(makeTranslucent: Boolean) {
        if (makeTranslucent) {
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

        } else {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        }
    }




    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    override fun update(o: Observable, arg: Any) {

    }
}
