package com.bouthaina.babbelcodechallengeproject.features.game

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.annotation.VisibleForTesting
import android.support.v7.widget.RecyclerView
import android.view.ViewTreeObserver
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.bouthaina.babbelcodechallengeproject.R
import com.bouthaina.babbelcodechallengeproject.databinding.GameActivityBinding
import com.bouthaina.babbelcodechallengeproject.features.BaseActivity
import java.util.Observable
import eightbitlab.com.blurview.BlurView
import eightbitlab.com.blurview.RenderScriptBlur


/**
 * Displays detailed information about the game
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

class GameActivity : BaseActivity() {

    @BindView(R.id.game_activity_recyclerview)  lateinit  var recyclerView: RecyclerView
    @BindView(R.id.game_activity_textCurrentWord)  lateinit  var currentWord: TextView
    @BindView(R.id.game_activity_textScore)  lateinit  var scoreNumber: TextView
    @BindView(R.id.game_activity_textfalling)  lateinit  var texttFalling: TextView
    @BindView(R.id.game_activity_backgroundForNewWord)  lateinit  var backgroundNewWord: FrameLayout
    @BindView(R.id.game_activity_linearlayoutcurrentword)  lateinit  var layoutForCurrentWord: LinearLayout
    @BindView(R.id.game_activity_linearlayoutgameover)  lateinit  var gameOverLayout: LinearLayout
    @BindView(R.id.game_activity_blurView)  lateinit  var blurView: BlurView

    private lateinit var gameActivityBinding: GameActivityBinding
    private lateinit var gameActivityViewModel: GameActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initDataBinding()
        setStatusBarAndNavBarTranslucent(false)
        setupObserver(gameActivityViewModel)
        setUpViews()
    }

    private fun initDataBinding() {
        gameActivityBinding = DataBindingUtil.setContentView(this, R.layout.game_activity)
        ButterKnife.bind(this)
        gameActivityViewModel = GameActivityViewModel(this,recyclerView, backgroundNewWord, currentWord, layoutForCurrentWord, scoreNumber, texttFalling)
        gameActivityBinding.viewModel = gameActivityViewModel
    }

    private fun setUpViews() {
        setUpBlurView()
        gameOverLayout.viewTreeObserver.addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
            override fun onPreDraw(): Boolean {
                gameOverLayout.viewTreeObserver.removeOnPreDrawListener(this)
                gameOverLayout.translationX = (-gameOverLayout.width - 10).toFloat()
                return true
            }
        })
    }

    private fun setUpBlurView() {
        val radius = 1.5f
        val decorView = window.decorView
        val windowBackground = decorView.background

        blurView.setupWith(findViewById(R.id.blurParent))
                .windowBackground(windowBackground)
                .blurAlgorithm(RenderScriptBlur(this, true))
                .blurRadius(radius)
    }

    override fun onPause() {
        gameActivityViewModel.pause()
        super.onPause()
    }

    override fun onDestroy() {
        gameActivityViewModel.destroy()
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        gameActivityViewModel.resume()
    }

    override fun update(o: Observable, arg: Any) {}

    private fun setupObserver(observable: Observable?) {
        observable!!.addObserver(this)
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    fun getViewModel():Observable {return gameActivityViewModel}
}
