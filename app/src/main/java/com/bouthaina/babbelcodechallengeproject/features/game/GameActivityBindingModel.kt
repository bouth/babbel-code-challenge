package com.bouthaina.babbelcodechallengeproject.features.game

import android.databinding.BaseObservable
import android.databinding.Bindable
import android.view.View
import com.bouthaina.babbelcodechallengeproject.BR
import com.bouthaina.babbelcodechallengeproject.R
import com.bouthaina.babbelcodechallengeproject.application.Application


/**
 * Two-way dataBinding model for [GameActivity]
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

class GameActivityBindingModel : BaseObservable() {

    @get:Bindable
    var txtHighScore :String = Application.getContext().getString(R.string.init_text_high_score)
        set(txtHighScore) {
            field = txtHighScore
            notifyPropertyChanged(BR.txtHighScore)
        }

    @get:Bindable
    var txtEndScore :String = Application.getContext().getString(R.string.init_text_end_score)
        set(txtEndScore) {
            field = txtEndScore
            notifyPropertyChanged(BR.txtEndScore)
        }

    @get:Bindable
    var visibiliy : Int = View.INVISIBLE
        set(visibiliy) {
            field = visibiliy
            notifyPropertyChanged(BR.visibiliy)
        }

}