package com.bouthaina.babbelcodechallengeproject.features.game

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.graphics.Point
import android.media.MediaPlayer
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.ViewTreeObserver
import android.view.WindowManager
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView

import com.bouthaina.babbelcodechallengeproject.R
import com.bouthaina.babbelcodechallengeproject.adapters.StarAdapter
import com.bouthaina.babbelcodechallengeproject.application.Application
import com.bouthaina.babbelcodechallengeproject.application.Singleton
import com.bouthaina.babbelcodechallengeproject.dataModel.WordModel
import com.bouthaina.babbelcodechallengeproject.dataModel.WordSetting
import kotlinx.android.synthetic.main.game_activity.*

import java.util.Observable
import java.util.concurrent.TimeUnit

import rx.Observer
import rx.Subscription
import rx.schedulers.Schedulers

/**
 * ViewModel for the [GameActivity]
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

open class GameActivityViewModel(gameActivity: GameActivity, var recycleView: RecyclerView, var backgroundNewWord: FrameLayout,
                                 var textCurrentWord: TextView, var layoutForCurrentWord: LinearLayout,
                                 var scoreNumber: TextView, var textFalling: TextView) : Observable() {

    var bindingModel = GameActivityBindingModel()
    private val context: Context
    private var gameActivity: GameActivity
    private var subscription: Subscription? = null
    private lateinit var starAdapter: StarAdapter
    private var currentWord: WordModel? = null
    private var fallingWord: WordModel? = null
    private var wordsSetting: WordSetting
    private var centerWordY: Int = 0
    private var topWordY: Int = 0
    private var fallingWordStartY: Int = 0
    private var screenHeight: Int = 0
    private var score: Int = 0
    private var correctSound: MediaPlayer
    private var incorrectSound: MediaPlayer

    init {
        this.context = gameActivity
        this.gameActivity = gameActivity
        recycleView.layoutManager = LinearLayoutManager(recycleView.context, LinearLayoutManager.HORIZONTAL, false)
        this.wordsSetting = WordSetting()
        this.wordsSetting.addAll(Singleton.getSingleton().wordSetting)

        textCurrentWord.viewTreeObserver.addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
            override fun onPreDraw(): Boolean {
                textCurrentWord.viewTreeObserver.removeOnPreDrawListener(this)
                val screenLocation = IntArray(2)
                textCurrentWord.getLocationOnScreen(screenLocation)
                centerWordY = screenLocation[1]
                return true
            }
        })

        layoutForCurrentWord.viewTreeObserver.addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
            override fun onPreDraw(): Boolean {
                layoutForCurrentWord.viewTreeObserver.removeOnPreDrawListener(this)
                val screenLocation = IntArray(2)
                layoutForCurrentWord.getLocationOnScreen(screenLocation)
                topWordY = screenLocation[1]
                return true
            }
        })
        textFalling.viewTreeObserver.addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
            override fun onPreDraw(): Boolean {
                textFalling.viewTreeObserver.removeOnPreDrawListener(this)
                val screenLocation = IntArray(2)
                textFalling.getLocationOnScreen(screenLocation)
                fallingWordStartY = screenLocation[1]
                return true
            }
        })

        val wm = Application.getContext().getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val size = Point()
        display.getSize(size)
        screenHeight = size.y
        correctSound = MediaPlayer.create(gameActivity, R.raw.correct)
        incorrectSound = MediaPlayer.create(gameActivity, R.raw.wrong)
    }

    fun onBackPressed() = gameActivity.onBackPressed()

    fun start() {
        score = 0
        starAdapter = StarAdapter(START_STARS)
        recycleView.adapter = starAdapter
        currentWord = wordsSetting.removeWord()
        fallingWord = wordsSetting.getTombstoneWord(currentWord!!)
        animateNewWord()
    }

    fun resume() {
        if (currentWord == null || fallingWord == null)
            start()
        else
            initiateCurrentWordFall()
    }

    private fun initiateCurrentWordFall() {
        val observable = rx.Observable.interval(40, TimeUnit.MILLISECONDS)
        subscription = observable.subscribeOn(Schedulers.computation())
                .observeOn(Schedulers.computation())
                .subscribe(object : Observer<Long> {

                    override fun onCompleted() {}

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                    }

                    override fun onNext(aLong: Long?) {
                        if (textFalling.y <= screenHeight) {
                            textFalling.y = textFalling.y + 1f + score.toFloat()
                            Log.d("Txt Y", textFalling.y.toString())
                        } else
                            gameActivity.runOnUiThread { wrongAnswerOrWordFeel() }
                    }
                })
    }

    private fun wrongAnswerOrWordFeel() {
        try {
            subscription!!.unsubscribe()
            incorrectSound.start()
            val lives = starAdapter.starts!! - 1
            starAdapter.starts = lives
            starAdapter.notifyItemRemoved(lives)
            currentWord = wordsSetting.removeWord()
            fallingWord = wordsSetting.getTombstoneWord(currentWord!!)
            if (lives > 0) {
                animateNewWord()
            } else {
                gameOver()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun gameOver() {
        textFalling.visibility = View.GONE
        bindingModel.txtEndScore = Application.getContext().getString(R.string.you_scored) + " " + score + " " + Application.getContext().getString(R.string.points) + "!"
        val highScore = Singleton.getSingleton().highScore
        if (score > highScore) {
            Singleton.getSingleton().highScore = score
            bindingModel.txtHighScore = Application.getContext().getString(R.string.new_high_score)
        } else {
            bindingModel.txtHighScore = Application.getContext().getString(R.string.high_score_is) + " " + highScore + " " + Application.getContext().getString(R.string.points)
        }
        bindingModel.visibiliy = View.VISIBLE
        gameActivity.game_activity_linearlayoutgameover.animate().translationX(0F).setDuration(750).setInterpolator(DecelerateInterpolator()).setListener(null).start()
    }

    private fun animateNewWord() {
        textFalling.y = fallingWordStartY.toFloat()
        textFalling.text = fallingWord!!.text_spa
        textCurrentWord.text = currentWord!!.text_eng
        backgroundNewWord.visibility = View.GONE
        backgroundNewWord.alpha = 0f
        textCurrentWord.visibility = View.GONE
        textCurrentWord.y = centerWordY.toFloat()
        textCurrentWord.alpha = 0f
        textCurrentWord.scaleX = 0.4f
        textCurrentWord.scaleY = 0.4f

        backgroundNewWord.visibility = View.VISIBLE
        backgroundNewWord.animate().alpha(1f).setDuration(1000).setListener(null).start()

        textCurrentWord.visibility = View.VISIBLE
        textCurrentWord.animate().setDuration(1000).setStartDelay(150).alpha(1f).scaleX(1f).scaleY(1f)
                .setInterpolator(AccelerateInterpolator())
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        super.onAnimationEnd(animation)
                        textCurrentWord.animate()
                                .y((topWordY - textCurrentWord.height / 5).toFloat())
                                .scaleX(0.6f).scaleY(0.6f).setInterpolator(AccelerateDecelerateInterpolator())
                                .setStartDelay(250).setDuration(750).setListener(object : AnimatorListenerAdapter() {
                                    override fun onAnimationEnd(animation: Animator) {
                                        super.onAnimationEnd(animation)
                                        initiateCurrentWordFall()
                                    }
                                }).start()
                        backgroundNewWord.animate().alpha(0f).setStartDelay(250).setDuration(750)
                                .setListener(object : AnimatorListenerAdapter() {
                                    override fun onAnimationEnd(animation: Animator) {
                                        super.onAnimationEnd(animation)
                                        backgroundNewWord.visibility = View.GONE
                                    }
                                })
                                .start()
                    }
                }).start()

    }


    fun pause() {
        if (subscription != null && !subscription!!.isUnsubscribed)
            subscription!!.unsubscribe()
    }

    fun restart() {
        this.wordsSetting.addAll(Singleton.getSingleton().wordSetting)
        textFalling.visibility = View.VISIBLE
        start()
    }

    fun destroy() {
        if (subscription != null && !subscription!!.isUnsubscribed)
            subscription!!.unsubscribe()
        incorrectSound.release()
        correctSound.release()
    }

    fun falseButtonIsHandle() {
        if (subscription != null && !subscription!!.isUnsubscribed) {
            if (!WordModel.isCorrectlyTranslated(currentWord!!, fallingWord!!))
                correctAnswer()
            else
                wrongAnswerOrWordFeel()
        }
    }

    private fun correctAnswer() {
        try {
            subscription!!.unsubscribe()
            correctSound.start()
            currentWord = wordsSetting.removeWord()
            fallingWord = wordsSetting.getTombstoneWord(currentWord!!)
            score++
            scoreNumber.text = score.toString()
            animateNewWord()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun playAgain() {
        gameActivity.game_activity_linearlayoutgameover.animate().translationX((-gameActivity.game_activity_linearlayoutgameover.width - 10).toFloat())
                .setDuration(750).setInterpolator(AccelerateInterpolator()).setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        super.onAnimationEnd(animation)
                        scoreNumber.text = 0.toString()
                        restart()
                    }
                }).start()
    }

    fun trueButtonIsHandle() {
        if (subscription != null && !subscription!!.isUnsubscribed) {
            if (WordModel.isCorrectlyTranslated(currentWord!!, fallingWord!!))
                correctAnswer()
            else
                wrongAnswerOrWordFeel()
        }
    }

    companion object {
        private val START_STARS = 3
    }

}