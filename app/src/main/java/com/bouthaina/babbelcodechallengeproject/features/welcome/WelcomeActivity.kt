package com.bouthaina.babbelcodechallengeproject.features.welcome

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.annotation.VisibleForTesting

import com.bouthaina.babbelcodechallengeproject.R

import com.bouthaina.babbelcodechallengeproject.databinding.WelcomeActivityBinding
import com.bouthaina.babbelcodechallengeproject.features.BaseActivity

import java.util.Observable

/**
 * Displays welcome screen
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

class WelcomeActivity : BaseActivity() {
    private lateinit var welcomeActivityViewModel: WelcomeActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initDataBinding()
        setStatusBarAndNavBarTranslucent(false)
        setupObserver(welcomeActivityViewModel)
    }

    private fun initDataBinding() {
        val welcomeActivityBinding = DataBindingUtil.setContentView<WelcomeActivityBinding>(this, R.layout.welcome_activity)
        welcomeActivityViewModel = WelcomeActivityViewModel(this)
        welcomeActivityBinding.mainViewModel = welcomeActivityViewModel
    }

    override fun update(o: Observable, arg: Any) {}

    fun setupObserver(observable: Observable?) = observable!!.addObserver(this)

    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    fun getViewModel():Observable {return welcomeActivityViewModel}
}
