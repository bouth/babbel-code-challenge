package com.bouthaina.babbelcodechallengeproject.features.welcome

import android.content.Context
import android.content.Intent

import com.bouthaina.babbelcodechallengeproject.application.Singleton
import com.bouthaina.babbelcodechallengeproject.dataModel.WordSetting
import com.bouthaina.babbelcodechallengeproject.features.game.GameActivity
import com.google.gson.Gson

import java.util.Observable

import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.functions.Func1
import rx.schedulers.Schedulers

/**
 * ViewModel for the [WelcomeActivity]
 *
 * @author Bouthaina TOUATI <bouthainatt@gmail.com>
 */

class WelcomeActivityViewModel(private val context: Context) : Observable() {

    fun startGame() = loadJsonWordFile("words.json")

    private fun loadJsonWordFile(fileName: String) {
        rx.Observable.just(fileName).map(Func1<String, WordSetting> { string ->
            try {
                val `is` = context.assets.open(string)
                val size = `is`.available()
                val buffer = ByteArray(size)
                `is`.read(buffer)
                `is`.close()
                val json = String(buffer, charset("UTF-8"))
                return@Func1 Gson().fromJson(json, WordSetting::class.java)

            } catch (e: Exception) {
                e.printStackTrace()
                return@Func1 null
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<WordSetting> {
                    override fun onCompleted() {}

                    override fun onError(e: Throwable) {}

                    override fun onNext(words: WordSetting) {
                        Singleton.getSingleton().wordSetting = words
                        val intent = Intent(context, GameActivity::class.java)
                        context.startActivity(intent)
                    }
                })
    }
}
